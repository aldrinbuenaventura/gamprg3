﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void loadScene(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
