﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cooldownIcon : MonoBehaviour
{
    private float startingCooldown = 0;
    private float currentCooldown;

    private void Update()
    {
        if(currentCooldown > 0)
        {
            currentCooldown -= Time.deltaTime;
            GetComponent<Image>().fillAmount = currentCooldown / startingCooldown;
        }
    }

    public void setFill(float cooldown)
    {
        startingCooldown = cooldown;
        currentCooldown = startingCooldown;
        GetComponent<Image>().fillAmount = 1;
    }
}
