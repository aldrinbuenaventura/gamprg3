﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TopGamePortrait : MonoBehaviour
{
    public TextMeshProUGUI respawnTimer;
    private float timer;

    private void Update()
    {
        if(respawnTimer.gameObject.activeSelf)
        {
            timer -= Time.deltaTime;
            respawnTimer.text = timer.ToString("F0");
            if (timer <= 0)
            {
                respawnTimer.gameObject.SetActive(false);
            }
        }
    }

    public void setupListeners(Hero hero)
    {
        hero.GetComponent<HealthComponent>().OnHeroDestroy.AddListener(onHeroDeath);
    }

    public void onHeroDeath(GameObject HeroGO)
    {
        timer = GameValues.RESPAWN_TIME[HeroGO.GetComponent<Level>().getLevel() - 1];
        respawnTimer.gameObject.SetActive(true);
        respawnTimer.text = timer.ToString();
    }
}
