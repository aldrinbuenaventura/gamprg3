﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopGameHUD : MonoBehaviour
{
    public List<Transform> portraitPositionRadiant;
    public List<Transform> portraitPositionDire;

    private int radiantIndex = 0;
    private int direIndex = 0;

    public void createTopPortrait(Hero hero)
    {
        Transform t;
        if (hero.GetComponent<Faction>().isRadiant)
        {
            t = portraitPositionRadiant[radiantIndex];
            radiantIndex++;
        }
        else
        {
            t = portraitPositionDire[direIndex];
            direIndex++;
        }
        GameObject go = Instantiate(PrefabManager.instance.topPortraitPrefab, t);

        go.GetComponent<TopGamePortrait>().setupListeners(hero);

    }


}
