﻿public static class GameValues
{
    public static int MELEE_COUNT = 3;
    public static int RANGED_COUNT = 1;
    public static int SIEGE_COUNT = 1;
    
    public static int DAY_CYCLE_TIME = 300;

    public static int CREEP_SPAWN_TIME = 30;
    public static int CREEP_SCALE_TIME = 450;
    public static int CREEP_SCALE_MULTIPLIER = 0;

    public static int CREEP_MELEE_INDEX = 0;
    public static int CREEP_RANGED_INDEX = 0;

    public static int TIER_ONE_BUILDING_INDEX = 0;
    public static int TIER_TWO_BUILDING_INDEX = 1;
    public static int TIER_THREE_BUILDING_INDEX = 2;

    public static bool RADIANT_ANCIENT_ONE_TOWER = false;
    public static bool DIRE_ANCIENT_ONE_TOWER = false;
    public static int RADIANT_TIER_FOUR_DESTROYED = 0;
    public static int DIRE_TIER_FOUR_DESTROYED = 0;

    public static bool RADIANT_TOP_MELEE_BARRACKS_DESTROYED = false;
    public static bool RADIANT_TOP_RANGED_BARRACKS_DESTROYED = false;
    public static bool RADIANT_MID_MELEE_BARRACKS_DESTROYED = false;
    public static bool RADIANT_MID_RANGED_BARRACKS_DESTROYED = false;
    public static bool RADIANT_BOT_MELEE_BARRACKS_DESTROYED = false;
    public static bool RADIANT_BOT_RANGED_BARRACKS_DESTROYED = false;

    public static bool DIRE_TOP_MELEE_BARRACKS_DESTROYED = false;
    public static bool DIRE_TOP_RANGED_BARRACKS_DESTROYED = false;
    public static bool DIRE_MID_MELEE_BARRACKS_DESTROYED = false;
    public static bool DIRE_MID_RANGED_BARRACKS_DESTROYED = false;
    public static bool DIRE_BOT_MELEE_BARRACKS_DESTROYED = false;
    public static bool DIRE_BOT_RANGED_BARRACKS_DESTROYED = false;

    public static int MAX_AI_DETECTION_TARGET = 10;
    public static float PROJECTILE_SPEED_VARIABLE = 100;

    public static float STARTING_GOLD = 600f;

    public static float[] EXP_REQUIREMENTS = {230, 600, 1080, 1660, 2260, 2980, 3730, 4510, 5320, 6160, 7030, 7930, 9155, 10405, 11680,
        12980, 14305, 15805, 17395, 18995, 20845, 22945, 25295, 27895, 9999};
    public static float[] EXP_BOUNTY = {55, 95, 150, 220, 295, 380, 475, 575, 680, 785, 895, 1010, 1150, 1311, 1475, 1640, 1815, 2000, 2200, 2405, 2630,
        2885, 3175, 3500, 3665};
    public static float[] RESPAWN_TIME = { 6, 8, 10, 14, 16, 26, 28, 30, 32, 34, 36, 44, 46, 48, 50, 52, 54, 65, 70, 75, 80, 85, 90, 95, 100 };


    public static int[] BASIC_SKILLS_LEVEL_REQUIREMENT = { 1, 3, 5, 7 };
    public static int[] ULTIMATE_SKILLS_LEVEL_REQUIREMENT = { 6, 12, 18};
}
