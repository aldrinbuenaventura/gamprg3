﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private float gameTimeInSeconds = 0;
    public float currentDayCycle;
    public Light directionalLightGO;

    //Variables for day cycles
    private enum timeCycle { DAY, NIGHT };
    private timeCycle currentCycle = timeCycle.DAY;

    private void Start()
    {
        currentDayCycle = GameValues.DAY_CYCLE_TIME;
        InvokeRepeating("updateUITime", 0.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.O))
        {
            Time.timeScale--;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale++;
        }


        gameTimeInSeconds += Time.deltaTime;
        currentDayCycle -= Time.deltaTime;

        //Check if time needs to change day cycle0F
        if (currentDayCycle <= 0)
        {
            currentDayCycle += GameValues.DAY_CYCLE_TIME;
            shiftTime();
        }

        if(gameTimeInSeconds > GameValues.CREEP_SCALE_TIME + GameValues.CREEP_SCALE_MULTIPLIER * GameValues.CREEP_SCALE_TIME)
        {
            GameValues.CREEP_SCALE_MULTIPLIER++;
        }
    }

    private void shiftTime()
    {
        switch (currentCycle)
        {
            case timeCycle.DAY:
                currentCycle = timeCycle.NIGHT;
                StartCoroutine("colorLerpCoroutine", new Color(0.78f, 0.78f, 1.0f));
                break;
            case timeCycle.NIGHT:
                currentCycle = timeCycle.DAY;
                StartCoroutine("colorLerpCoroutine", new Color(1.0f, 1.0f, 1.0f));
                break;

        }
    }

    IEnumerator colorLerpCoroutine(Color c)
    {
        float lerp = 0;

        while (lerp < 1)
        {
            lerp = Mathf.MoveTowards(lerp, 1, Time.deltaTime / 0.1f);
            directionalLightGO.GetComponent<Light>().color = Color.Lerp(directionalLightGO.GetComponent<Light>().color, c, 0.1f);
        }
        yield return null;
    }

    private void updateUITime()
    {
        UIManager.instance.updateTimeText(gameTimeInSeconds);
    }
}
