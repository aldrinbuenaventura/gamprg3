﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject playerGO;

    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField]
    private TeamManager RadiantManager;
    [SerializeField]
    private TeamManager DireManager;



    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            if (playerGO.GetComponent<Level>().getLevel() == 25) return;
            playerGO.GetComponent<Level>().addEXP(GameValues.EXP_REQUIREMENTS[playerGO.GetComponent<Level>().getLevel() - 1]);
        }
    }

    public void setPlayerGO(GameObject go)
    {
        playerGO = go;
    }

    public GameObject getPlayerGO()
    {
        return playerGO;
    }

    public void addHero(Hero hero)
    {
        if (hero.GetComponent<Faction>().isRadiant) RadiantManager.addHero(hero);
        else DireManager.addHero(hero);

        UIManager.instance.callScorecardCreation(hero);
        UIManager.instance.callTopGameHUDCreation(hero);
    }
}
