﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TeamManager : MonoBehaviour
{
    public List<GameObject> Towers;
    public List<GameObject> Barracks;
    public GameObject Ancient;

    public List<Hero> Heroes;
    public List<SpawnerWaypoints> SpawnerGO;

    public Vector3 spawnPoint;


    private void Start()
    {
        foreach (GameObject tower in Towers) tower.GetComponent<Tower>().OnDestruction.AddListener(onTowerDestroy);
        foreach (GameObject barrack in Barracks) barrack.GetComponent<Barrack>().OnBarracksDestroy.AddListener(onBarracksDestroy);
        Ancient.GetComponent<Ancient>().OnAncientDestroy.AddListener(onAncientDestroy);


        foreach (Hero hero in Heroes)
        {
            hero.GetComponent<HealthComponent>().OnHeroDestroy.AddListener(onHeroDeath);
            hero.GetComponent<HealthComponent>().OnHeroKill.AddListener(onHeroKill);
            hero.OnHeroRespawn.AddListener(setLane);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            foreach (GameObject tower in Towers) if (tower.GetComponent<Tower>().tier == Tower.Tiers.ONE) Destroy(tower);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            foreach (GameObject tower in Towers) if (tower.GetComponent<Tower>().tier == Tower.Tiers.TWO) Destroy(tower);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            foreach (GameObject tower in Towers) if (tower.GetComponent<Tower>().tier == Tower.Tiers.THREE) Destroy(tower);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            foreach (GameObject tower in Towers) if (tower.GetComponent<Tower>().tier == Tower.Tiers.FOUR) Destroy(tower);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            foreach (GameObject barrack in Barracks) Destroy(barrack);
        }

    }

    //Event for tower destroy
    private void onTowerDestroy(GameObject towerGO)
    {
        foreach (Hero hero in Heroes)
        {
            hero.GetComponent<Gold>().addGold(towerGO.GetComponent<Reward>().getTeamGold());
        }

        foreach (GameObject tower in Towers)
        {
            if (tower == null) return;
            if (tower.GetComponent<Lane>().lane == towerGO.GetComponent<Lane>().lane || tower.GetComponent<Lane>().lane == Lane.Lanes.NONE)
            {
                if (towerGO.GetComponent<Tower>().tier == Tower.Tiers.ONE)
                {
                    if (tower.GetComponent<Tower>().tier == Tower.Tiers.TWO) tower.GetComponent<HealthComponent>().setInvulverable(false);
                }
                else if (towerGO.GetComponent<Tower>().tier == Tower.Tiers.TWO)
                {
                    if (tower.GetComponent<Tower>().tier == Tower.Tiers.THREE) tower.GetComponent<HealthComponent>().setInvulverable(false);
                }
                else if (towerGO.GetComponent<Tower>().tier == Tower.Tiers.THREE)
                {
                    if (tower.GetComponent<Tower>().tier == Tower.Tiers.FOUR) tower.GetComponent<HealthComponent>().setInvulverable(false);
                    foreach (GameObject barrack in Barracks) barrack.GetComponent<HealthComponent>().setInvulverable(false);
                }
            }
        }

        if (towerGO.GetComponent<Tower>().tier == Tower.Tiers.FOUR)
        {
            if (checkFaction())
            {
                GameValues.RADIANT_TIER_FOUR_DESTROYED++;
                if (GameValues.RADIANT_TIER_FOUR_DESTROYED >= 2) Ancient.GetComponent<HealthComponent>().setInvulverable(false);
            }
            else
            {
                GameValues.DIRE_TIER_FOUR_DESTROYED++;
                if (GameValues.DIRE_TIER_FOUR_DESTROYED >= 2) Ancient.GetComponent<HealthComponent>().setInvulverable(false);
            }
        }

        foreach (GameObject tower in Towers) if (tower == towerGO) { Towers.Remove(tower); return; }
    }

    //Event for barracks destroy
    private void onBarracksDestroy(GameObject barracksGO)
    {
        foreach (Hero hero in Heroes)
        {
            hero.GetComponent<Gold>().addGold(barracksGO.GetComponent<Reward>().getTeamGold());
        }

        foreach (GameObject barracks in Barracks)
        {
            if (barracks == null) return;
            if (barracks.GetComponent<Lane>().lane == barracksGO.GetComponent<Lane>().lane)
            {
                //check for specifics (type of barrack destroyed + faction)
                switch (barracks.GetComponent<Lane>().lane)
                {
                    case Lane.Lanes.TOP:
                        if (barracksGO.GetComponent<Barrack>().barrackType == Barrack.BARRACK_TYPE.MELEE)
                        {
                            if (checkFaction()) GameValues.DIRE_TOP_MELEE_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_TOP_MELEE_BARRACKS_DESTROYED = true;
                        }
                        else
                        {
                            if (checkFaction()) GameValues.DIRE_TOP_RANGED_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_BOT_RANGED_BARRACKS_DESTROYED = true;
                        }
                        break;
                    case Lane.Lanes.MID:
                        if (barracksGO.GetComponent<Barrack>().barrackType == Barrack.BARRACK_TYPE.MELEE)
                        {
                            if (checkFaction()) GameValues.DIRE_MID_MELEE_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_MID_MELEE_BARRACKS_DESTROYED = true;
                        }
                        else
                        {
                            if (checkFaction()) GameValues.DIRE_MID_RANGED_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_MID_RANGED_BARRACKS_DESTROYED = true;
                        }
                        break;
                    case Lane.Lanes.BOTTOM:
                        if (barracksGO.GetComponent<Barrack>().barrackType == Barrack.BARRACK_TYPE.MELEE)
                        {
                            if (checkFaction()) GameValues.DIRE_BOT_MELEE_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_BOT_MELEE_BARRACKS_DESTROYED = true;
                        }
                        else
                        {
                            if (checkFaction()) GameValues.DIRE_MID_RANGED_BARRACKS_DESTROYED = true;
                            else GameValues.RADIANT_MID_RANGED_BARRACKS_DESTROYED = true;
                        }
                        break;
                }
            }
        }

        foreach (GameObject barrack in Barracks) if (barrack == barracksGO) { Barracks.Remove(barrack); return; }
    }

    private void onAncientDestroy(GameObject ancientGO)
    {
        if (ancientGO.GetComponent<Faction>().isRadiant) UIManager.instance.displayEndGame("Dire");
        else UIManager.instance.displayEndGame("Radiant");
    }

    private bool checkFaction()
    {
        return GetComponent<Faction>().isRadiant;
    }

    public void addHero(Hero hero)
    {
        Heroes.Add(hero);
    }

    public void onHeroDeath(GameObject HeroGO)
    {
        HeroGO.GetComponent<kdaStats>().addDeath();
        HeroGO.transform.position = spawnPoint;
        gameObject.AddComponent<RespawnComponent>().setRespawn(GameValues.RESPAWN_TIME[HeroGO.GetComponent<Level>().getLevel() - 1], HeroGO);
    }

    public void onHeroKill(GameObject HeroGO)
    {
        HeroGO.GetComponent<kdaStats>().addKill();
    }

    public void setLane(GameObject HeroGO)
    {
        if (HeroGO.GetComponent<Creep>())
        {
            int rand = Random.Range(0, SpawnerGO.Count);
            HeroGO.GetComponent<Creep>().setWaypoints(SpawnerGO[rand].Waypoints);
        }
    }
}
