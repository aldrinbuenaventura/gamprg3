﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileHit: UnityEvent<HealthComponent> { }

public class Projectile : MonoBehaviour
{

    public ProjectileHit OnHit = new ProjectileHit();

    public virtual void onSpawn(GameObject targetGO, float spd)
    {
        transform.LookAt(targetGO.transform);

    }
    public virtual void onSpawnLinear(Vector3 t, float spd) { }

}
