﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetedProjectile : Projectile
{
    public float speed;
    private GameObject target;

    private void Update()
    {
        if (target == null || !target.activeSelf) Destroy(gameObject);
        else transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
    }

    public override void onSpawn(GameObject targetGO, float spd)
    {
        base.onSpawn(targetGO, spd);
        target = targetGO;
        speed = spd;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == target)
        {
            OnHit.Invoke(other.gameObject.GetComponent<HealthComponent>());
            Destroy(gameObject);
        }
    }
}


