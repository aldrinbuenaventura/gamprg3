﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class LinearProjectile : Projectile
{
    public float speed;
    private Vector3 lastPosition;
    private Vector3 target;

    private void Start()
    {
        GetComponent<SphereCollider>().radius = SkillValues.WAVE_OF_TERROR_RADIUS / 50;
        lastPosition = transform.position;
        transform.LookAt(target);
}

    private void Update()
    {
        if (target == Vector3.zero) Destroy(gameObject);
        else
        {
            
            transform.position += transform.forward * Time.deltaTime * speed;
            if (Vector3.Distance(lastPosition, transform.position) > SkillValues.WAVE_OF_TERROR_TRAVEL_DISTANCE / 50) Destroy(gameObject);
        }
    }

    public override void onSpawnLinear(Vector3 t, float spd)
    {
        target = t;
        speed = spd;

        GetComponent<SphereCollider>().radius = SkillValues.WAVE_OF_TERROR_RADIUS / 50;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Unit>()) OnHit.Invoke(other.gameObject.GetComponent<HealthComponent>());
    }
}
