﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScrolling : MonoBehaviour
{
    //Adjustable Variables
    public float scrollSpeed;
    public float rectSize;

    //Cap distance for camera scrolling
    public float topLimit;
    public float botLimit;
    public float leftLimit;
    public float rightLimit;

    //Rectangles for camera scrolling
    private Rect rectUp;
    private Rect rectDown;
    private Rect rectLeft;
    private Rect rectRight;


    private void Start()
    {
        if (GameManager.instance.getPlayerGO().GetComponent<Faction>().isRadiant) { transform.position = new Vector3(-80, 30, -100); }
        else { transform.position = new Vector3(80, 30, 70); }
        //Initialize rectangles
        rectUp = new Rect(0, Screen.height - rectSize, Screen.width, rectSize);
        rectDown = new Rect(0, 0, Screen.width, rectSize);
        rectLeft = new Rect(0, 0, rectSize, Screen.height);
        rectRight = new Rect(Screen.width - rectSize, 0, rectSize, Screen.height);
    }

    // Update is called once per frame
    void Update()
    {
        //Scroll up
        if (transform.position.z < topLimit)
        {
            if (rectUp.Contains(Input.mousePosition))
            {
                transform.Translate(Vector3.forward * Time.deltaTime * scrollSpeed, Space.World);
            }
        }

        //Scroll Down
        if (transform.position.z > botLimit)
        {
            if (rectDown.Contains(Input.mousePosition))
            {
                transform.Translate(Vector3.back * Time.deltaTime * scrollSpeed, Space.World);
            }
        }

        //Scroll Left
        if (transform.position.x > leftLimit)
        {
            if (rectLeft.Contains(Input.mousePosition))
            {
                transform.Translate(Vector3.left * Time.deltaTime * scrollSpeed, Space.World);
            }
        }

        //Scroll Right
        if (transform.position.x < rightLimit)
        {
            if (rectRight.Contains(Input.mousePosition))
            {
                transform.Translate(Vector3.right * Time.deltaTime * scrollSpeed, Space.World);
            }
        }

    }
}
