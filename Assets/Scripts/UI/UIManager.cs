﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIManager : MonoBehaviour
{
    [Header("Canvas")]
    [SerializeField]
    private GameObject detailedStatsCanvas;
    [SerializeField]
    private GameObject endGameCanvas;
    [SerializeField]
    private GameObject scoreboardCanvas;
    [SerializeField]
    private GameObject topGameHUDCanvas;

    [Header("Win Screen")]
    public TextMeshProUGUI winText;

    [Header("Stats")]
    public TextMeshProUGUI[] AttackText;
    public TextMeshProUGUI[] ArmorText;
    public TextMeshProUGUI[] SpeedText;
    public TextMeshProUGUI STRText;
    public TextMeshProUGUI AGIText;
    public TextMeshProUGUI INTText;

    public TextMeshProUGUI AttackSpeedText;
    public TextMeshProUGUI AttackRangeText;
    public TextMeshProUGUI SpellAmpText;
    public TextMeshProUGUI ManaRegenText;
    public TextMeshProUGUI PResistText;
    public TextMeshProUGUI MResistText;
    public TextMeshProUGUI EvasionText;
    public TextMeshProUGUI HealthRegenText;

    [Header("Top Game HUD")]
    public TextMeshProUGUI timer;

    [Header("Ability")]
    public GameObject[] skillButtons;
    public cooldownIcon[] skillCooldown;

    [Header("Bars")]
    public Slider hpSlider;
    public Slider mpSlider;

    [Header("EXP")]
    public TextMeshProUGUI LevelText;
    public Image RadialExpBar;


    public static UIManager instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GameManager.instance.getPlayerGO().GetComponent<Level>().OnExpGain.AddListener(updateExpUI);
        GameManager.instance.getPlayerGO().GetComponent<HealthComponent>().OnHealthChange.AddListener(updateHpUI);
        GameManager.instance.getPlayerGO().GetComponent<ManaComponent>().OnManaChange.AddListener(updateMpUI);
        //GameManager.instance.getPlayerGO().GetComponent<AttackComponent>().OnAttackChange.AddListener(updateRangeUI);
        GameManager.instance.getPlayerGO().GetComponent<DefenseComponent>().OnDefenseChange.AddListener(updateArmorText);
        hpSlider.maxValue = GameManager.instance.getPlayerGO().GetComponent<HealthComponent>().getMaxHP();
        mpSlider.maxValue = GameManager.instance.getPlayerGO().GetComponent<ManaComponent>().getMaxMP();
        
        GameManager.instance.getPlayerGO().GetComponent<AbilityManager>().OnSkillPointChange.AddListener(updateSkillButtons);

        GameManager.instance.getPlayerGO().GetComponent<AbilityManager>().OnCooldown.AddListener(updateCooldownUI);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.BackQuote)) scoreboardCanvas.SetActive(true);
        if (Input.GetKeyUp(KeyCode.BackQuote)) scoreboardCanvas.SetActive(false);
    }

    public void updateUIText(float attack, float armor, float mspd, float STR, float AGI, float INT, float aspd, float range, float spellAmp,
        float manaRegen, float mArmor, float evasion, float hRegen, float maxHP, float maxMP)
    {

        foreach (TextMeshProUGUI t in AttackText) t.text = attack.ToString("F0");
        foreach (TextMeshProUGUI t in ArmorText) t.text = armor.ToString("F0");
        foreach (TextMeshProUGUI t in SpeedText) t.text = mspd.ToString("F0");
        STRText.text = STR.ToString("F0");
        AGIText.text = AGI.ToString("F0");
        INTText.text = INT.ToString("F0");
        
        AttackSpeedText.text = aspd.ToString();
        AttackRangeText.text = range.ToString();
        SpellAmpText.text = spellAmp.ToString() + "%";
        ManaRegenText.text = manaRegen.ToString();
        PResistText.text = ((0.052 * armor) / (0.9 + 0.048 * Mathf.Abs(armor))).ToString("F2") + "%";
        MResistText.text = (1-(1 * (1 - mArmor))).ToString() + "%";
        EvasionText.text = evasion.ToString() + "%";
        HealthRegenText.text = hRegen.ToString();

        hpSlider.maxValue = maxHP;
        mpSlider.maxValue = maxMP;
    }

    public void updateRangeUI(float range)
    {
        Debug.Log(range);
        AttackRangeText.text = range.ToString();
    }

    public void updateArmorText(float armor)
    {
        foreach (TextMeshProUGUI t in ArmorText) t.text = armor.ToString("F0");
        PResistText.text = ((0.052 * armor) / (0.9 + 0.048 * Mathf.Abs(armor))).ToString("F2") + "%";
    }

    public void updateExpUI(int level, float currentExp)
    {
        LevelText.text = level.ToString();
        if (level == 25) RadialExpBar.fillAmount = 1;
        RadialExpBar.fillAmount = currentExp / GameValues.EXP_REQUIREMENTS[level - 1];
    }


    public void displayDetailedStats()
    {
        detailedStatsCanvas.SetActive(true);
    }
    public void hideDetailedStats()
    {
        detailedStatsCanvas.SetActive(false);
    }

    public void displayEndGame(string teamName)
    {
        endGameCanvas.SetActive(true);
        winText.text = teamName + " Win";
    }

    public void callScorecardCreation(Hero hero)
    {
        scoreboardCanvas.GetComponent<Scoreboard>().createScorecard(hero);
    }
    public void callTopGameHUDCreation(Hero hero)
    {
        topGameHUDCanvas.GetComponent<TopGameHUD>().createTopPortrait(hero);
    }

    public void updateHpUI(float currentHP)
    {
        hpSlider.value = currentHP;
    }
    public void updateMpUI(float currentMP)
    {
        mpSlider.value = currentMP;
    }

    public void addSkillPoint(int index)
    {
        GameManager.instance.getPlayerGO().GetComponent<AbilityManager>().rankSkill(index);
    }

    public void updateSkillButtons(int i)
    {
        if (i <= 0) foreach (GameObject button in skillButtons) button.SetActive(false);
        else foreach (GameObject button in skillButtons) button.SetActive(true);
    }

    public void updateCooldownUI(float cooldown, int index)
    {
        skillCooldown[index].setFill(cooldown);
    }

    public void updateTimeText(float t)
    {
        var ts = TimeSpan.FromSeconds(t);
        timer.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
    }
}
