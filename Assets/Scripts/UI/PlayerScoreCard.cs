﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
public class PlayerScoreCard : MonoBehaviour
{
    public Image portrait;
    public TextMeshProUGUI netWorthText;
    public TextMeshProUGUI LevelText;
    public TextMeshProUGUI killText;
    public TextMeshProUGUI deathText;

    public void Start()
    {
        
    }

    public void setPortrait(Sprite s)
    {
        portrait.sprite = s;
    }

    public void setNetWorth(float netWorth)
    {
        netWorthText.text = netWorth.ToString("F0");
    }

    public void setLevel(int level)
    {
        Debug.Log("XD");
        LevelText.text = "Level " + level.ToString("F0");
    }

    public void setKill(float k, bool b)
    {
        killText.text = k.ToString("F0");
    }

    public void setDeath(float d)
    {
        deathText.text = d.ToString("F0");
    }

    public void setupListeners(Hero hero)
    {
        hero.GetComponent<kdaStats>().OnDeathChange.AddListener(setDeath);
        hero.GetComponent<kdaStats>().OnKillChange.AddListener(setKill);
        hero.GetComponent<Gold>().OnGoldAdd.AddListener(setNetWorth);
        hero.GetComponent<Level>().OnLevelUp.AddListener(setLevel);
    }

}
