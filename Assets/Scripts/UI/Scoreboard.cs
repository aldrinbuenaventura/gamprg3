﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scoreboard : MonoBehaviour
{
    public List<Transform> scoreboardPositionRadiant;
    public List<Transform> scoreboardPositionDire;

    public TextMeshProUGUI radiantTotalText;
    public TextMeshProUGUI direTotalText;

    private int radiantIndex = 0;
    private int direIndex = 0;

    public void createScorecard(Hero hero)
    {
        Transform t;
        if (hero.GetComponent<Faction>().isRadiant)
        {
            t = scoreboardPositionRadiant[radiantIndex];
            radiantIndex++;
        }
        else
        {
            t = scoreboardPositionDire[direIndex];
            direIndex++;
        }
        GameObject go = Instantiate(PrefabManager.instance.scorecardPrefab, t);

        go.GetComponent<PlayerScoreCard>().setupListeners(hero);
        
        hero.GetComponent<kdaStats>().OnKillChange.AddListener(setKill);
    }

    public void setKill(float a, bool isRadiant)
    {
        float i;
        if (isRadiant)
        {
            i = float.Parse(radiantTotalText.text);
            i++;
            radiantTotalText.text = i.ToString();
        }
        else
        { 
            i = float.Parse(direTotalText.text);
            i++;
            direTotalText.text = i.ToString();
        }
    }
}
