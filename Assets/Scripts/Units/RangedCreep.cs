﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RangedCreep : Creep
{
    public GameObject ProjectilePrefab;


    public override void Attack()
    {
        base.Attack();
        if (AIDetectionComponent.target != null && attackDelay <= 0)
        {
            attackDelay = GetComponent<AttackComponent>().getBAT();

            Projectile projectile = Instantiate(ProjectilePrefab.GetComponent<Projectile>(), transform.position, Quaternion.identity);
            projectile.OnHit.AddListener(onProjectileHit);
            projectile.onSpawn(AIDetectionComponent.target, 1000 / GameValues.PROJECTILE_SPEED_VARIABLE);

        }
    }

    private void onProjectileHit(HealthComponent targetHP)
    {
        targetHP.TakeDamage(GetComponent<AttackComponent>().getDamage(), "Physical", targetHP.GetComponent<DefenseComponent>(), gameObject);
    }
}
