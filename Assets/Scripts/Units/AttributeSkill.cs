﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeSkill : Skills
{
    private void Start()
    {
        setMaxRank(SkillValues.ATTRIBUTE_MAX_RANK);
    }

    public override void onRankUp()
    {

        if (rank >= maxRank) return;
        GetComponent<AbilityManager>().useSkillPoint();
        rank++;
        GetComponent<HeroStats>().addAttributes(2);
    }
}
