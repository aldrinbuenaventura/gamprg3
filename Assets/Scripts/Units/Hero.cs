﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HeroKill : UnityEvent<GameObject> { }
public class HeroDie : UnityEvent<GameObject> { }
public class HeroEarn : UnityEvent<GameObject> { }
public class HeroRespawn : UnityEvent<GameObject> { }
public class Hero : MonoBehaviour
{

    public HeroKill OnHeroKill = new HeroKill();
    public HeroDie OnHeroDie = new HeroDie();
    public HeroEarn OnHeroEarn = new HeroEarn();
    public HeroRespawn OnHeroRespawn = new HeroRespawn();
    
    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.addHero(this);        
    }


}
