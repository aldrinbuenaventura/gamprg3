﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AIDetection : MonoBehaviour
{
    int layer_mask;

    public Collider[] targets;
    public List<GameObject> inRange = new List<GameObject>();

    public GameObject target;

    float sightRangeWire;
    // Start is called before the first frame update
    void Start()
    {
        layer_mask = LayerMask.GetMask("Unit", "Building");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FindClosestTarget(float sightRange)
    {
        sightRangeWire = sightRange;
        targets = Physics.OverlapSphere(this.gameObject.transform.position, sightRange, layer_mask);
        inRange.Clear();
        foreach(var target in targets)
        {
            if (target.GetComponent<Faction>().isRadiant != this.GetComponent<Faction>().isRadiant && !target.GetComponent<HealthComponent>().getInvulnerable()) inRange.Add(target.gameObject);
        }

        if (inRange.Count == 0) { target = null; return; }
        target = inRange.OrderBy(e => (e.transform.position - transform.position).magnitude).First();

        target.GetComponent<HealthComponent>().OnUnitDestroy.AddListener(removeTarget);
    }

    public void removeTarget(GameObject go)
    {
        target = null;
    }

    void OnDrawGizmosSelected()
    {
        if (sightRangeWire == 0) return;
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, sightRangeWire);
    }
}
