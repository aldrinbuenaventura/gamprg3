﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedHero : HeroControl
{
    public GameObject ProjectilePrefab;

    public override void Attack()
    {
        base.Attack();
        spawnProjectile();
    }

    public override void holdAttack()
    {
        base.holdAttack();
        spawnProjectile();
    }

    public void spawnProjectile()
    {
        if (AIDetectionComponent.target != null && attackDelay <= 0)
        {
            GetComponent<Animation>().Play("attack");
            attackDelay = GetComponent<HeroStats>().baseAttackTime;

            Projectile projectile = Instantiate(ProjectilePrefab.GetComponent<Projectile>(), transform.position, Quaternion.identity);
            projectile.OnHit.AddListener(onProjectileHit);
            projectile.onSpawn(AIDetectionComponent.target, GetComponent<HeroStats>().projectileSpeed / GameValues.PROJECTILE_SPEED_VARIABLE);

        }
    }

    private void onProjectileHit(HealthComponent targetHP)
    {
        targetHP.TakeDamage(GetComponent<AttackComponent>().getDamage(), "Physical" , targetHP.GetComponent<DefenseComponent>(), gameObject);
    }
}
