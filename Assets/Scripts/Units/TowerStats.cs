﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerStats : MonoBehaviour
{
    [Header("Base stats")]
    public float baseHealth;
    public float armor;
    public float baseAttackDamage;
    public float baseAttackTime;
    public float attackRange;
    public float projectileSpeed;
    public float teamGoldBounty;
    public float lastHitGoldBounty;
    public float towerProtectionArmorBonus; 
    public float hpRegen;
    public float magicResistance;

    private float currentHealth;

    public float getBaseHealth()
    {
        return baseHealth;
    }
    public float getCurrentHealth()
    {
        return currentHealth;
    }
    public float getCurrentAttackDamage()
    {
        return baseAttackDamage;
    }
    public float getHPRegen()
    {
        return hpRegen;
    }

    public float getArmor() { return armor; }
    public float getMagicArmor() { return magicResistance; }
}
