﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HeroStats : MonoBehaviour
{
    public enum Primary_Attributes
    {
        AGILITY,
        STRENGTH,
        INTELLIGENCE,
    };
    [Header("Base stats")]
    public Primary_Attributes primaryAttribute;
    public float strength;
    public float strengthGrowth;
    public float agility;
    public float agilityGrowth;
    public float intelligence;
    public float intelligenceGrowth;
    public float health;
    public float healthRegen;
    public float magicResistance;
    public float mana;
    public float manaRegen;
    public float armor;
    public float damage;
    public float movementSpeed;
    public float attackSpeed;
    public float attackRange;
    public float projectileSpeed;
    public float baseAttackTime;

    public bool isRanged;

    private float currentDamage;
    private float currentMaxHP;
    private float currentHpRegen;
    private float currentMaxMP;
    private float currentMpRegen;
    private float currentArmor;
    private float currentMagicRes;
    private float bonusSpellDamage;
    private float currentAspd;
    private float currentMspd;
    private float evasion;

    private float netWorth;

    AttackComponent attackComponent;
    DefenseComponent defenseComponent;

    private void Awake()
    {
        attackComponent = GetComponent<AttackComponent>();
        defenseComponent = GetComponent<DefenseComponent>();

        updateSubStats();
        updateComponents();
        attackComponent.setRange(attackRange);
        attackComponent.setRangeType(isRanged);
        attackComponent.setRange(attackRange);
        attackComponent.setBAT(baseAttackTime);
        attackComponent.setSight(750);
        GetComponent<Level>().OnLevelUp.AddListener(onLevelUp);
    }

    private void Start()
    {
        updateSubStats();
        updateComponents();
        attackComponent.setRange(attackRange);
        attackComponent.setRangeType(isRanged);
        attackComponent.setRange(attackRange);
        attackComponent.setBAT(baseAttackTime);
        attackComponent.setSight(750);
        updateUI();
        
    }

    public void onLevelUp(int level)
    {
        strength += strengthGrowth;
        agility += agilityGrowth;
        intelligence += intelligenceGrowth;
        updateAll();
    }

    public void updateAll()
    {
        updateSubStats();
        updateComponents();
        updateUI();
    }

    public void updateSubStats()
    {
        if (primaryAttribute == Primary_Attributes.AGILITY) currentDamage = damage + agility;
        if (primaryAttribute == Primary_Attributes.INTELLIGENCE) currentDamage = damage + intelligence;
        if (primaryAttribute == Primary_Attributes.STRENGTH) currentDamage = damage + strength;

        currentMaxHP = health + strength * 20;
        currentHpRegen = healthRegen + strength * 0.1f;
        currentMagicRes = magicResistance + strength * 0.08f;
        currentArmor = armor + agility * 0.16f;
        currentAspd = attackSpeed + agility;
        currentMspd = movementSpeed + agility * 0.05f;
        currentMaxMP = mana + intelligence * 12f;
        currentMpRegen = manaRegen + intelligence * 0.05f;
        bonusSpellDamage = intelligence * 0.07f;

    }

    public void updateComponents()
    {
        GetComponent<HealthComponent>().setHP(currentMaxHP, currentHpRegen);
        GetComponent<ManaComponent>().setMP(currentMaxMP, currentMpRegen);
        GetComponent<DefenseComponent>().setDefenses(currentArmor, currentMagicRes);
        GetComponent<AttackComponent>().setDamage(currentDamage);
        GetComponent<AttackComponent>().setBonusSpellDamage(bonusSpellDamage);
        GetComponent<NavMeshAgent>().speed = currentMspd / 100;
        GetComponent<Reward>().setGoldReward(110 + (GetComponent<Level>().getLevel() * 8));
        GetComponent<Reward>().setExpReward(GameValues.EXP_BOUNTY[GetComponent<Level>().getLevel() - 1]);
    }

    public void updateUI()
    {
        if (GetComponent<HeroControl>()) UIManager.instance.updateUIText(attackComponent.getDamage(), defenseComponent.getTotalArmor(), currentMspd, strength,
            agility, intelligence, currentAspd,
            attackComponent.getRange(), attackComponent.getBonusSpellDamage(), currentMpRegen, defenseComponent.getMagicArmor(), defenseComponent.getEvasion(),
            currentHpRegen, currentMaxHP, currentMaxMP);
    }

    public float getCurrentAttackDamage()
    {
        return currentDamage;
    }

    public void addPrimaryAttribute(float add)
    {
        if (primaryAttribute == Primary_Attributes.STRENGTH) strength += add;
        if (primaryAttribute == Primary_Attributes.AGILITY) agility += add;
        if (primaryAttribute == Primary_Attributes.INTELLIGENCE) intelligence += add;
        updateAll();
    }

    public void addAttributes(float add)
    {
        strength += add;
        agility += add;
        intelligence += add;

        updateAll();
    }

    public void onRespawn()
    {
        GetComponent<HealthComponent>().setToMax();
        GetComponent<ManaComponent>().setToMax();

        updateAll();
    }
}
