﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepStats : MonoBehaviour
{
    [Header("Base stats")]
    public float baseHealth;
    public float armor;
    public float movespeed;
    public float baseAttackDamage;
    public float baseAttackTime;
    public float attackRange;
    public float sightRange;
    public float baseGoldBounty;
    public float baseExperience;
    public float magicResistance;

    public float healthGrowth;
    public float attackDamageGrowth;
    public float goldBountyGrowth;
    public float experienceGrowth;

    public bool isRanged;

    private float currentHealth;
    private float currentArmor;
    private float currentAttackDamage;
    private float currentGoldBounty;
    private float currentExperience;
    private float healthRegen;

    [Header("Super stats")]
    public float superBaseHealth;
    public float superBaseArmor;
    public float superBaseAttackDamage;
    public float superBaseGoldBounty;
    public float superBaseExperience;

    public float superHealthGrowth;
    public float superAttackDamageGrowth;
    public float superGoldBountyGrowth;
    public float superExperienceGrowth;

    public void scaleStats(bool isSuper)
    {
        if (isSuper)
        {
            currentHealth = superBaseHealth + superHealthGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentArmor = superBaseArmor;
            currentAttackDamage = superBaseAttackDamage + superAttackDamageGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentGoldBounty = superBaseGoldBounty + superGoldBountyGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentExperience = superBaseExperience + superExperienceGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
        }
        else
        {
            currentArmor = armor;
            currentHealth = baseHealth + healthGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentAttackDamage = baseAttackDamage + attackDamageGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentGoldBounty = baseGoldBounty + goldBountyGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
            currentExperience = baseExperience + experienceGrowth * GameValues.CREEP_SCALE_MULTIPLIER;
        }
        GetComponent<DefenseComponent>().setDefenses(armor, magicResistance);
        GetComponent<AttackComponent>().setDamage(currentAttackDamage);
        GetComponent<AttackComponent>().setRange(attackRange);
        GetComponent<AttackComponent>().setSight(sightRange);
        GetComponent<AttackComponent>().setBAT(baseAttackTime);
        GetComponent<AttackComponent>().setRangeType(isRanged);
        GetComponent<Reward>().setGoldReward(currentGoldBounty);
        GetComponent<Reward>().setExpReward(currentExperience);
    }

    public float getCurrentHealth()
    {
        return currentHealth;
    }
    public float getCurrentAttackDamage()
    {
        return currentAttackDamage;
    }
    public float getCurrentGoldBounty()
    {
        return currentGoldBounty;
    }
    public float getCurrentExperience()
    {
        return currentExperience;
    }
    public float getHPRegen()
    {
        return healthRegen;
    }
}
