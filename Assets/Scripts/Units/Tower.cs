﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TowerDestroyed : UnityEvent<GameObject> { }
public class Tower : Building
{
    public TowerDestroyed OnDestruction = new TowerDestroyed();
    public GameObject ProjectilePrefab;

    AIDetection AIDetectionComponent;
    public float attackDelay = 0;

    public enum Tiers
    {
        ONE,
        TWO,
        THREE,
        FOUR
    };
    public Tiers tier;

    public enum TowerStates
    {
        IDLE, ATTACK
    }
    public TowerStates towerState;

    private void OnDestroy()
    {
        OnDestruction.Invoke(this.gameObject);
    }

    private void Awake()
    {
        GetComponent<HealthComponent>().setHP(GetComponent<TowerStats>().getBaseHealth(), GetComponent<TowerStats>().getHPRegen());
        GetComponent<DefenseComponent>().setDefenses(GetComponent<TowerStats>().getArmor(), GetComponent<TowerStats>().getMagicArmor());
        GetComponent<AttackComponent>().setDamage(GetComponent<TowerStats>().getCurrentAttackDamage());
        GetComponent<Reward>().setGoldReward(GetComponent<TowerStats>().lastHitGoldBounty);
        GetComponent<Reward>().setTeamGold(GetComponent<TowerStats>().teamGoldBounty);
    }

    private void Start()
    {
        AIDetectionComponent = GetComponent<AIDetection>();

        
        GetComponent<TowerProtection>().onActivate();

        InvokeRepeating("scanForEnemies", 0.0f, 1.0f);
    }

    private void Update()
    {
        if (AIDetectionComponent.target != null) towerState = TowerStates.ATTACK; else towerState = TowerStates.IDLE;
        switch (towerState)
        {
            case TowerStates.IDLE:
                break;
            case TowerStates.ATTACK:
                Attack();
                break;
        }
    }

    //Attack
    private void Attack()
    {
        attackDelay -= Time.deltaTime;

        if (AIDetectionComponent.target != null && attackDelay <= 0)
        {
            attackDelay = GetComponent<TowerStats>().baseAttackTime;

            Projectile projectile = Instantiate(ProjectilePrefab.GetComponent<Projectile>(), transform.position, Quaternion.identity);
            projectile.OnHit.AddListener(onProjectileHit);
            projectile.onSpawn(AIDetectionComponent.target, GetComponent<TowerStats>().projectileSpeed / GameValues.PROJECTILE_SPEED_VARIABLE);

        }
    }

    public void scanForEnemies()
    {
        if (GetComponent<AIDetection>()) AIDetectionComponent.FindClosestTarget(GetComponent<TowerStats>().attackRange / 50);
    }

    private void onProjectileHit(HealthComponent targetHP)
    {
        targetHP.TakeDamage(GetComponent<AttackComponent>().getDamage(), "Physical", targetHP.GetComponent<DefenseComponent>(), gameObject);
    }
}
