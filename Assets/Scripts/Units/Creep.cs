﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creep : MonoBehaviour
{
    public enum states
    {
        IDLE, MOVE, CHASE, ATTACK, STUNNED
    }
    public states state;

    public List<Transform> waypoints = new List<Transform>();
    private int currentWaypointIndex = 0;

    protected AIDetection AIDetectionComponent;
    protected StatusCondition statusConditonComponent;

    public float attackDelay = 0;

    public float rotationspeed = 5;
    private float angle;
    private Vector3 rotateToTransform;

    private void Awake()
    {

        AIDetectionComponent = GetComponent<AIDetection>();
        statusConditonComponent = GetComponent<StatusCondition>();
    }

    private void Start()
    {
        state = states.IDLE;
        InvokeRepeating("scanForEnemies", 0.0f, 1.0f);
        if (GetComponent<CreepStats>())
            GetComponent<HealthComponent>().setHP(GetComponent<CreepStats>().getCurrentHealth(), GetComponent<CreepStats>().getHPRegen());
    }

    private void Update()
    {
        if (AIDetectionComponent.target && state != states.ATTACK) state = states.CHASE;
        if (AIDetectionComponent.target == null && waypoints.Count != 0) state = states.MOVE;
        if (statusConditonComponent.getStunned()) state = states.STUNNED;

        switch (state) {
            case states.MOVE:
                moveWaypoints();
                break;
            case states.CHASE:
                chaseTarget();
                break;
            case states.ATTACK:
                Attack();
                break;
            case states.STUNNED:
                Stunned();
                break;
        }

    }

    //Move
    public void moveWaypoints()
    {
        this.gameObject.GetComponent<NavMeshAgent>().isStopped = false;
        if (Vector3.Distance(this.transform.position, waypoints[currentWaypointIndex].position) <= 1f)
        {
            if(currentWaypointIndex < waypoints.Count) currentWaypointIndex++;
        }
        this.gameObject.GetComponent<NavMeshAgent>().SetDestination(waypoints[currentWaypointIndex].position);
    }
    //Chase
    public void chaseTarget()
    {
        this.gameObject.GetComponent<NavMeshAgent>().isStopped = false;
        this.gameObject.GetComponent<NavMeshAgent>().SetDestination(AIDetectionComponent.target.transform.position);

        if (Vector3.Distance(this.transform.position, AIDetectionComponent.target.transform.position) < GetComponent<AttackComponent>().getRange() / 40) state = states.ATTACK;
        if (Vector3.Distance(transform.position, AIDetectionComponent.target.transform.position) > GetComponent<AttackComponent>().getSight() / 50) state = states.MOVE;

    }

    //Attack
    public virtual void Attack()
    {
        rotateToTransform = AIDetectionComponent.target.transform.position;

        Vector3 _direction = (rotateToTransform - transform.position).normalized;
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);
        angle = Quaternion.Angle(transform.rotation, _lookRotation);

        angle = Quaternion.Angle(transform.rotation, _lookRotation);
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationspeed);

        if (Vector3.Distance(this.transform.position, AIDetectionComponent.target.transform.position) > GetComponent<AttackComponent>().getRange() / 40) state = states.CHASE;

        this.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
        attackDelay -= Time.deltaTime;

    }

    //Search for enemies
    public void scanForEnemies()
    {
        if (state != states.MOVE) return;
        if (this.GetComponent<AIDetection>()) AIDetectionComponent.FindClosestTarget(GetComponent<AttackComponent>().getSight() / 50);
    }

    public void Stunned()
    {
        AIDetectionComponent.target = null;
        this.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
    }

    //Grow Stats

    public void onSpawn(List<Transform> wp, bool isSuper)
    {
        this.GetComponent<CreepStats>().scaleStats(isSuper);
        waypoints = wp;
    }

    public void setWaypoints(List<Transform> wp)
    {
        waypoints = wp;
    }
}
