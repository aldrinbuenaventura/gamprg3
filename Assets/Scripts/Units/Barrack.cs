﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BarrackDestroyed : UnityEvent<GameObject> { }
public class Barrack : Building
{
    public BarrackDestroyed OnBarracksDestroy = new BarrackDestroyed();
    public enum BARRACK_TYPE
    {
        MELEE,
        RANGED
    };
    public BARRACK_TYPE barrackType;

    private void Start()
    {
        GetComponent<HealthComponent>().setHP(GetComponent<TowerStats>().getBaseHealth(), GetComponent<TowerStats>().getHPRegen());
        GetComponent<Reward>().setGoldReward(GetComponent<TowerStats>().lastHitGoldBounty);
        GetComponent<Reward>().setTeamGold(GetComponent<TowerStats>().teamGoldBounty);
    }

    private void OnDestroy()
    {
        OnBarracksDestroy.Invoke(gameObject);
    }
}
