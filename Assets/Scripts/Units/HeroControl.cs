﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HeroControl : Hero
{
    int layer_mask;
    public enum heroStates
    {
        MOVE, CHASE, ATTACK, TURNING, STOP, HOLD, CASTING
    }
    public heroStates heroState;

    public float rotationspeed;
    private float angle;
    public float attackDelay;

    public Vector3 moveLocation;
    private Vector3 rotateToTransform;
    NavMeshAgent agent;

    public AIDetection AIDetectionComponent;
    [SerializeField]
    private GameObject indicator;


    private void Start()
    {
        AIDetectionComponent = GetComponent<AIDetection>();
        layer_mask = LayerMask.GetMask("Ground", "Building", "Unit");
        heroState = heroStates.STOP;
        agent = GetComponent<NavMeshAgent>();
        GameManager.instance.setPlayerGO(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        attackDelay -= Time.deltaTime;
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, layer_mask))
            {
                CancelCommand();

                if (hit.collider.tag == "Ground")
                {
                    indicator.transform.position = hit.point;
                    indicator.GetComponent<Animator>().Play("Base Layer.Waypointer");
                    moveLocation = hit.point;
                }
                else
                {
                    Debug.Log(hit.collider.gameObject);
                    if (hit.collider.gameObject.GetComponent<HealthComponent>())
                    {
                        if (hit.collider.gameObject.GetComponent<HealthComponent>().getInvulnerable() ||
                            hit.collider.gameObject.GetComponent<Faction>().isRadiant == GetComponent<Faction>().isRadiant) { heroState = heroStates.STOP; return; }
                        AIDetectionComponent.target = hit.collider.gameObject;
                    }
                }
                heroState = heroStates.TURNING;
            }
        }
        switch (heroState)
        {
            case heroStates.MOVE:
                MoveHero();
                break;
            case heroStates.CHASE:
                chaseTarget();
                if (AIDetectionComponent.target == null) heroState = heroStates.STOP;
                break;
            case heroStates.ATTACK:
                Attack();
                if (AIDetectionComponent.target == null) heroState = heroStates.STOP;
                break;
            case heroStates.TURNING:
                RotateToTarget();
                break;
            case heroStates.STOP:
                CancelCommand();
                scanForEnemies();
                break;
            case heroStates.HOLD:
                holdAttack();
                break;
        }
    }

    private void MoveHero()
    {
        GetComponent<Animation>().Play("walk");
        agent.SetDestination(moveLocation);
        if (Vector3.Distance(transform.position, agent.destination) <= 1f)
        {
            heroState = heroStates.STOP;
        }
    }

    private void RotateToTarget()
    {
        if (moveLocation != Vector3.zero) rotateToTransform = moveLocation;
        else if (AIDetectionComponent.target != null) rotateToTransform = AIDetectionComponent.target.transform.position;

        Vector3 _direction = (rotateToTransform - transform.position).normalized;
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);
        angle = Quaternion.Angle(transform.rotation, _lookRotation);

        angle = Quaternion.Angle(transform.rotation, _lookRotation);
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationspeed);

        if (angle <= 50f)
        {
            if (moveLocation != Vector3.zero) heroState = heroStates.MOVE;
            else if (AIDetectionComponent.target != null) heroState = heroStates.CHASE;
        }
    }
    
    private void CancelCommand()
    {
        GetComponent<Animation>().Play("free");

        agent.SetDestination(transform.position);
        moveLocation = Vector3.zero;
        AIDetectionComponent.target = null;
    }

    public void chaseTarget()
    {
        GetComponent<Animation>().Play("walk");
        RotateToTarget();
        this.gameObject.GetComponent<NavMeshAgent>().SetDestination(AIDetectionComponent.target.transform.position);

        if (Vector3.Distance(this.transform.position, AIDetectionComponent.target.transform.position) <= GetComponent<HeroStats>().attackRange / 40)
        {
            heroState = heroStates.ATTACK;
        }

    }

    //Attack
    public virtual void Attack()
    {
        if (AIDetectionComponent.target == null) heroState = heroStates.STOP;
        agent.SetDestination(transform.position);
        if (Vector3.Distance(this.transform.position, AIDetectionComponent.target.transform.position) > GetComponent<HeroStats>().attackRange / 40) heroState = heroStates.CHASE;
    }

    public void scanForEnemies()
    {
        if (GetComponent<AIDetection>()) AIDetectionComponent.FindClosestTarget(GetComponent<HeroStats>().attackRange / 40);
        if (AIDetectionComponent.target != null) heroState = heroStates.HOLD;
    }

    public virtual void holdAttack()
    {
        GetComponent<Animation>().Play("attack");
        if (AIDetectionComponent.target != null)
        {
            if (Vector3.Distance(transform.position, AIDetectionComponent.target.transform.position) > GetComponent<HeroStats>().attackRange / 40) heroState = heroStates.STOP;
        }
        else heroState = heroStates.STOP;

    }
}
