﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AncientDestroyed : UnityEvent<GameObject> { }
public class Ancient : Building
{
    public AncientDestroyed OnAncientDestroy = new AncientDestroyed();

    private void Awake()
    {
        GetComponent<HealthComponent>().setHP(GetComponent<TowerStats>().getBaseHealth(), GetComponent<TowerStats>().getHPRegen());
    }

    private void OnDestroy()
    {
        OnAncientDestroy.Invoke(gameObject);
    }
}
