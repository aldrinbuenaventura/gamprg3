﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCreep : Creep
{
    public override void Attack()
    {
        base.Attack();
        if (AIDetectionComponent.target != null && attackDelay <= 0)
        {
            attackDelay = GetComponent<CreepStats>().baseAttackTime;
            AIDetectionComponent.target.GetComponent<HealthComponent>().TakeDamage(GetComponent<AttackComponent>().getDamage(), "Physical", AIDetectionComponent.target.GetComponent<DefenseComponent>(), gameObject);
        }
    }
}
