﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerWaypoints : MonoBehaviour
{
    public List<Transform> Waypoints = new List<Transform>();


    public bool checkBarracks(int index)
    {
        if (GetComponent<Faction>().isRadiant)
        {
            if (GetComponent<Lane>().lane == Lane.Lanes.TOP)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.DIRE_TOP_MELEE_BARRACKS_DESTROYED;
                else return GameValues.DIRE_TOP_RANGED_BARRACKS_DESTROYED;
            }
            if (GetComponent<Lane>().lane == Lane.Lanes.MID)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.DIRE_MID_MELEE_BARRACKS_DESTROYED;
                else return GameValues.DIRE_MID_RANGED_BARRACKS_DESTROYED;
            }
            if (GetComponent<Lane>().lane == Lane.Lanes.BOTTOM)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.DIRE_BOT_MELEE_BARRACKS_DESTROYED;
                else return GameValues.DIRE_BOT_RANGED_BARRACKS_DESTROYED;
            }
        } else
        {
            if (GetComponent<Lane>().lane == Lane.Lanes.TOP)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.RADIANT_TOP_MELEE_BARRACKS_DESTROYED;
                else return GameValues.RADIANT_TOP_RANGED_BARRACKS_DESTROYED;
            }
            if (GetComponent<Lane>().lane == Lane.Lanes.MID)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.RADIANT_MID_MELEE_BARRACKS_DESTROYED;
                else return GameValues.RADIANT_MID_RANGED_BARRACKS_DESTROYED;
            }
            if (GetComponent<Lane>().lane == Lane.Lanes.BOTTOM)
            {
                if (index == GameValues.CREEP_MELEE_INDEX) return GameValues.RADIANT_BOT_MELEE_BARRACKS_DESTROYED;
                else return GameValues.RADIANT_BOT_RANGED_BARRACKS_DESTROYED;
            }
        }
        return false;
    }
}
