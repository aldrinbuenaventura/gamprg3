﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageNumber : MonoBehaviour
{
    public void destroyNumber()
    {
        Debug.Log("XD");
    }

    public void updateText(float damage)
    {
        GetComponentInChildren<TextMeshProUGUI>().text = "-" + damage.ToString();
        Invoke("destroyAfterFewSeconds", 1.0f);
    }

    public void destroyAfterFewSeconds()
    {
        Destroy(this.gameObject);
        
    }
}
