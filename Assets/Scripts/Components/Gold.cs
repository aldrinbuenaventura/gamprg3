﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onGoldAdd : UnityEvent<float> { }
public class Gold : MonoBehaviour
{
    public onGoldAdd OnGoldAdd = new onGoldAdd();
    public float netWorth;

    private void Start()
    {
        netWorth = GameValues.STARTING_GOLD;
    }

    public void addGold(float gold)
    {
        netWorth += gold;
        OnGoldAdd.Invoke(netWorth);
    }
    public float getGold()
    {
        return netWorth;
    }
}
