﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnComponent : MonoBehaviour
{
    public float respawnTimer;
    public GameObject heroGO;

    // Update is called once per frame
    void Update()
    {
        respawnTimer -= Time.deltaTime;
        if(respawnTimer <= 0)
        {
            heroGO.SetActive(true);
            heroGO.GetComponent<HeroStats>().onRespawn();
            heroGO.GetComponent<AIDetection>().target = null;
            heroGO.GetComponent<Hero>().OnHeroRespawn.Invoke(heroGO);
            Destroy(this);
        }
    }

    public void setRespawn(float t, GameObject go)
    {
        respawnTimer = t;
        heroGO = go;
    }
}
