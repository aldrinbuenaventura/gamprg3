﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onExpGain : UnityEvent<int, float> { }
public class onLevelUp : UnityEvent<int> { }
public class Level : MonoBehaviour
{
    public onExpGain OnExpGain = new onExpGain();
    public onLevelUp OnLevelUp = new onLevelUp();
    public int level = 1;
    public float currentEXP;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<HeroControl>())
        {
            UIManager.instance.updateExpUI(level, currentEXP);
        }
    }

    public void addEXP(float expGained)
    {
        if (level == 25) return;
        currentEXP += expGained;
        if(currentEXP >= GameValues.EXP_REQUIREMENTS[level - 1])
        {
            currentEXP -= GameValues.EXP_REQUIREMENTS[level - 1];
            level++;
            if (GetComponent<AbilityManager>()) GetComponent<AbilityManager>().addSkillPoints();
            //GetComponent<HeroStats>().onLevelUp(level);
            OnLevelUp.Invoke(level);
        }

        OnExpGain.Invoke(level, currentEXP);
    }

    public int getLevel()
    {
        return level;
    }
}
