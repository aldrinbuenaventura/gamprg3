﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepSpawner : MonoBehaviour
{
    //Waypoints and spawnpoints
    public List<Transform> spawnPoints = new List<Transform>();

    
    public GameObject meleeCreep;
    public GameObject rangeCreep;
    public GameObject siegeCreep;

    private float timeToSpawn = 0;
    private int wavesSpawned = 0;
    
    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToSpawn >= GameValues.CREEP_SPAWN_TIME)
        {
            StartCoroutine(spawnWave());
            spawnWave();
            timeToSpawn = 0;
        }
        else
        {
            timeToSpawn += Time.deltaTime;
        }

        if(Input.GetKeyDown(KeyCode.Alpha7))
        {
            StartCoroutine(spawnWave());
            spawnWave();
            timeToSpawn = 0;
        }
    }


    //Spawn wave when called
    private IEnumerator spawnWave()
    {

        wavesSpawned++;
        //SPAWN
        Creep creepSpawned;
        for (int i = 0; i < GameValues.MELEE_COUNT; i++)
        {
            foreach (var s in spawnPoints)
            {
                creepSpawned = Instantiate(meleeCreep.GetComponent<Creep>(), s.position, Quaternion.identity);
                creepSpawned.GetComponent<Creep>().onSpawn(s.GetComponent<SpawnerWaypoints>().Waypoints,
                    s.GetComponent<SpawnerWaypoints>().checkBarracks(GameValues.CREEP_MELEE_INDEX));
            }

            yield return new WaitForSeconds(1f);

        }
        for (int i = 0; i < GameValues.RANGED_COUNT; i++)
        {
            foreach (var s in spawnPoints)
            {
                creepSpawned = Instantiate(rangeCreep.GetComponent<Creep>(), s.position, Quaternion.identity);
                creepSpawned.GetComponent<Creep>().onSpawn(s.GetComponent<SpawnerWaypoints>().Waypoints,
                    s.GetComponent<SpawnerWaypoints>().checkBarracks(GameValues.CREEP_RANGED_INDEX));
            }
            yield return new WaitForSeconds(1f);
        }

        if(wavesSpawned % 10 == 0)
        {
            for (int i = 0; i < GameValues.SIEGE_COUNT; i++)
            {
                foreach (var s in spawnPoints)
                {
                    creepSpawned = Instantiate(siegeCreep.GetComponent<Creep>(), s.position, Quaternion.identity);
                    creepSpawned.GetComponent<Creep>().onSpawn(s.GetComponent<SpawnerWaypoints>().Waypoints,
                        s.GetComponent<SpawnerWaypoints>().checkBarracks(GameValues.CREEP_RANGED_INDEX));
                }
                yield return new WaitForSeconds(1f);
            }
        }

    }
}
