﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnitDestroyed : UnityEvent<GameObject> { }
public class HeroDestroyed : UnityEvent<GameObject> { }
public class onHeroKill : UnityEvent<GameObject> { }
public class onHealthChange : UnityEvent<float> { }
public class HealthComponent : MonoBehaviour
{
    public UnitDestroyed OnUnitDestroy = new UnitDestroyed();
    public HeroDestroyed OnHeroDestroy = new HeroDestroyed();
    public onHeroKill OnHeroKill = new onHeroKill();
    public onHealthChange OnHealthChange = new onHealthChange();

    public float currentHP;
    private float maxHP;
    public float healthRegen;
    private float damageDone;
    [SerializeField]
    private bool isInvulnerable;

    private void Start()
    {
        currentHP = maxHP;
    }

    private void Update()
    {
        currentHP += healthRegen * Time.deltaTime;
        currentHP = Mathf.Clamp(currentHP, 0, maxHP);
        OnHealthChange.Invoke(currentHP);
        GetComponentInChildren<HealthBar>().updateHealthSlider(currentHP, maxHP);
    }


    public void TakeDamage(float damage, string damageType, DefenseComponent defenseS, GameObject attacker)
    {
        if (!isInvulnerable)
        {
            AttackComponent attackS = attacker.GetComponent<AttackComponent>();
            if (damageType == "Physical")
            {
                if (checkForEvasion(defenseS)) return;

                double damageMultiplier;
                damageMultiplier = 1 - ((0.052 * defenseS.getTotalArmor()) / (0.9 + 0.048 * Mathf.Abs(defenseS.getTotalArmor())));
                damageDone = damage * (float)damageMultiplier;
                currentHP -= damageDone;
            }
            else if(damageType == "Magical")
            {
                double damageMultiplier;
                damageMultiplier = 1 * (1 + defenseS.getMagicArmor());
                damageDone = damage * (float)damageMultiplier;
                currentHP -= damageDone;
            }


            if (attackS.gameObject.tag == "HeroPlayer")
            {
                GameObject go = Instantiate(PrefabManager.instance.damagePrefab, defenseS.transform.position, Quaternion.identity);
                go.GetComponent<DamageNumber>().updateText((int)damageDone);
            }
            GetComponentInChildren<HealthBar>().updateHealthSlider(currentHP, maxHP);

            if (currentHP <= 0)
            {
                OnUnitDestroy.Invoke(gameObject);

                //Give Exp/Gold reward
                if (GetComponent<Reward>())
                {
                    GetComponent<Reward>().giveReward(attacker, defenseS);
                }

                if (GetComponent<Hero>())
                {
                    if (attacker.GetComponent<Hero>())
                    {
                        OnHeroKill.Invoke(attacker);
                    }
                    GetComponent<Hero>().OnHeroDie.Invoke(gameObject);
                    OnHeroDestroy.Invoke(gameObject);
                    gameObject.SetActive(false);

                }
                else Destroy(gameObject);
            }
        }
    }

    public bool checkForEvasion(DefenseComponent defenseS)
    {
        float totalEvasion = defenseS.getEvasion();
        if (totalEvasion == 0) return false;

        if (totalEvasion > Random.Range(0, 100f)) return true;
        else return false;
    }


    public void setHP(float HP, float hpRegen)
    {
        maxHP = HP;
        healthRegen = hpRegen;
    }

    public float getMaxHP()
    {
        return maxHP;
    }

    public void setInvulverable(bool b)
    {
        isInvulnerable = b;
    }

    public bool getInvulnerable()
    {
        return isInvulnerable;
    }

    public void setToMax()
    {
        currentHP = maxHP;
    }
}
