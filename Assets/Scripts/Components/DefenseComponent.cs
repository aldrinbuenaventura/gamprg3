﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onDefenseChange : UnityEvent<float> { }

public class DefenseComponent : MonoBehaviour
{
    public onDefenseChange OnDefenseChange = new onDefenseChange();

    public float armor;
    private float magicArmor;
    private float evasion;

    public float bonusArmor;
    public void setArmor(float a)
    {
        armor = a;
    }
    public void setMagicArmor(float a)
    {
        magicArmor = a;
    }
    public void setEvasion(float a)
    {
        evasion = a;
    }
    public void setDefenses(float _armor, float _mArmor)
    {
        armor = _armor;
        magicArmor = _mArmor;
    }

    public float getArmor() { return armor; }
    public float getTotalArmor() { return armor + bonusArmor; }
    public float getMagicArmor() { return magicArmor; }
    public float getEvasion() { return evasion; }

    public void addArmor(float a)
    {
        armor += a;
    }
    public void addBonusArmor(float a)
    {
        bonusArmor += a;
        OnDefenseChange.Invoke(armor + bonusArmor);
    }

}
