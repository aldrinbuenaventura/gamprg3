﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onKillChange : UnityEvent<float, bool> { }
public class onDeathChange : UnityEvent<float> { }
public class kdaStats : MonoBehaviour
{
    public onKillChange OnKillChange = new onKillChange();
    public onDeathChange OnDeathChange = new onDeathChange();
    public float kills;
    public float deaths;

    private void Start()
    {
    }

    public float getKills()
    {
        return kills;
    }

    public float getDeaths()
    {
        return deaths;
    }

    public void addDeath()
    {
        deaths++;
        OnDeathChange.Invoke(deaths);
    }
    public void addKill()
    {
        kills++;
        OnKillChange.Invoke(kills, GetComponent<Faction>().isRadiant);
    }
}
