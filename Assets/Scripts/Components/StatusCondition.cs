﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusCondition : MonoBehaviour
{
    private bool isStunned;

       
    public void setStunned(bool b)
    {
        isStunned = b;
    }

    public bool getStunned() { return isStunned; }
}
