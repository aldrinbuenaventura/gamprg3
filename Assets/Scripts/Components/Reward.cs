﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward : MonoBehaviour
{
    private float expReward;
    private float goldReward;
    private float teamGoldReward;

    int layer_mask;

    public List<Hero> heroesInRange = new List<Hero>();
    private void Start()
    {
        layer_mask = LayerMask.GetMask("Unit");
    }

    public void setExpReward(float exp)
    {
        expReward = exp;
    }
    public void setGoldReward(float gold)
    {
        goldReward = gold;
    }
    public void setTeamGold(float teamGold)
    {
        teamGoldReward = teamGold;
    }

    public float getExpReward()
    {
        return expReward;
    }
    public float getGoldReward()
    {
        return goldReward;
    }
    public float getTeamGold()
    {
        return teamGoldReward;
    }


    public void giveReward(GameObject attacker, DefenseComponent deadUnit)
    {
        Collider[] Units;
        Units = Physics.OverlapSphere(this.gameObject.transform.position, 1500 / 50, layer_mask);
        heroesInRange.Clear();
        foreach (var unit in Units)
        {
            if (unit.GetComponent<Hero>())
            {
                if (unit.GetComponent<Faction>().isRadiant != GetComponent<Faction>().isRadiant)
                {
                    heroesInRange.Add(unit.GetComponent<Hero>());
                }
            }
        }
        foreach (var hero in heroesInRange)
        {
            hero.GetComponent<Level>().addEXP(expReward / heroesInRange.Count);
        }

        giveGoldReward(attacker, deadUnit.GetComponent<Reward>());

    }

    public void giveGoldReward(GameObject attacker, Reward deadUnitReward)
    {
        if (deadUnitReward.GetComponent<Hero>())
        {
            Collider[] Units;
            Units = Physics.OverlapSphere(this.gameObject.transform.position, 1300 / 50, layer_mask);
            foreach (var unit in Units)
            {
                if (unit.GetComponent<Hero>())
                {
                    if (unit.GetComponent<Faction>().isRadiant != GetComponent<Faction>().isRadiant)
                    {
                        if (attacker.GetComponent<Hero>())
                        {
                            if (attacker.GetComponent<Hero>() == unit.GetComponent<Hero>()) unit.GetComponent<Gold>().addGold(deadUnitReward.getGoldReward());
                            else unit.GetComponent<Gold>().addGold(.25f * deadUnitReward.getGoldReward());
                        }
                        else { unit.GetComponent<Gold>().addGold(.25f * deadUnitReward.getGoldReward()); }
                        
                    }
                }
            }
        }
        else if(attacker.GetComponent<Gold>()) attacker.GetComponent<Gold>().addGold(deadUnitReward.getGoldReward());
    }
}
