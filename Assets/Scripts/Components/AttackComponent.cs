﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onAttackChange : UnityEvent<float> { }

public class AttackComponent : MonoBehaviour
{
    public onAttackChange OnAttackChange = new onAttackChange();

    private float damage;
    private float bonusSpellDamage;
    private float sightRange;
    private float baseAttackTime;
    private bool isRanged;
    public float attackRange = 750;

    public void setDamage(float a) { damage = a; }
    public float getDamage() { return damage; }
    public void setBonusSpellDamage(float a) { bonusSpellDamage = a; }
    public float getBonusSpellDamage() { return bonusSpellDamage; }
    public void setSight(float a) { sightRange = a; }
    public float getSight() { return sightRange; }
    public void setRange(float a) { attackRange = a; }
    public float getRange() { return attackRange; }
    public void setBAT(float a) { baseAttackTime = a; }
    public float getBAT() { return baseAttackTime; }
    public void addRange(float a)
    {
        attackRange += a;
        OnAttackChange.Invoke(attackRange);
    }
    public void setRangeType(bool b) { isRanged = b; }
    public bool getRangeType() { return isRanged; }


}
