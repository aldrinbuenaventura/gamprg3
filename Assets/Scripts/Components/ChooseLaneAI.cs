﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseLaneAI : MonoBehaviour
{
    private void Start()
    {
        Invoke("startMoving", 25f);
    }

    public void startMoving()
    {

        GetComponent<Hero>().OnHeroRespawn.Invoke(gameObject);
    }

}
