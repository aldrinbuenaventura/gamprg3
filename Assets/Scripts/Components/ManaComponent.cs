﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onManaChange : UnityEvent<float> { }
public class ManaComponent : MonoBehaviour
{

    public onManaChange OnManaChange = new onManaChange();

    private float currentMP;
    private float maxMP;
    private float manaRegen;

    private void Start()
    {
        currentMP = maxMP;
    }

    private void Update()
    {
        currentMP += manaRegen * Time.deltaTime;
        currentMP = Mathf.Clamp(currentMP, 0, maxMP);
        OnManaChange.Invoke(currentMP);
    }

    public void setMP(float MP, float mpRegen)
    {
        maxMP = MP;
        manaRegen = mpRegen;
    }

    public float getMaxMP()
    {
        return maxMP;
    }

    public bool checkForManaCost(float cost)
    {
        if (cost > currentMP) return false;
        else
        {
            currentMP -= cost;
            return true;
        }
    }

    public void setToMax()
    {
        currentMP = maxMP;
    }

}
