﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicMissile : TargetedActiveSkill
{
    public GameObject ProjectilePrefab;
    public GameObject StunEffect;

    public override void lookForTarget()
    {
        base.lookForTarget();
    }

    private void Start()
    {
        setMaxRank(SkillValues.MAGIC_MISSILE_MAX_RANK);
    }

    public override void onActivate()
    {

        if (target.GetComponent<Unit>())
        {
            if (target.GetComponent<Faction>().isRadiant != GetComponent<Faction>().isRadiant)
            {
                Debug.Log("CASTABLE");
                //GetComponent<AIDetection>().target = target;

                if (Vector3.Distance(target.transform.position, gameObject.transform.position) <= SkillValues.MAGIC_MISSILE_CAST_RANGE / 50)
                {
                    if (!GetComponent<ManaComponent>().checkForManaCost(SkillValues.MAGIC_MISSILE_MANA_COST[rank])) return;
                    base.onActivate();

                    cooldownTime = SkillValues.MAGIC_MISSILE_COOLDOWN[rank];
                    Projectile projectile = Instantiate(ProjectilePrefab.GetComponent<Projectile>(), transform.position, Quaternion.identity);
                    projectile.OnHit.AddListener(onProjectileHit);
                    projectile.onSpawn(target, SkillValues.MAGIC_MISSILE_PROJECTILE_SPEED / GameValues.PROJECTILE_SPEED_VARIABLE);
                }
                else return;
            }
            else
            {
                Debug.Log("CANT TARGET ALLIES");
            }
        }
    }

    public void onProjectileHit(HealthComponent targetHP)
    {
        targetHP.TakeDamage(SkillValues.MAGIC_MISSILE_DAMAGE[rank], "Magical" , targetHP.GetComponent<DefenseComponent>(), gameObject);

        //Apply Stun Buff
        BuffReceiver buffReceiver = target.GetComponent<BuffReceiver>();
        if (buffReceiver)
        {
            Buff buff = Instantiate(StunEffect.GetComponent<Buff>());
            buff.setDuration(SkillValues.MAGIC_MISSILE_STUN_DURATION[rank]);
            buffReceiver.ApplyBuff(buff);
        }
    }
}
