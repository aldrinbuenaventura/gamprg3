﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectionAuraBuff : Buff
{
    protected override void OnActivate(BuffReceiver target)
    {
        Debug.Log(target);
        target.GetComponent<DefenseComponent>().addBonusArmor(SkillValues.TOWER_PROTECTION_AURA_BONUS[rank]);
    }
    protected override void OnDeactivate()
    {
        Destroy(Target.GetComponent<TowerProtected>());
        Target.GetComponent<DefenseComponent>().addBonusArmor(-SkillValues.TOWER_PROTECTION_AURA_BONUS[rank]);
    }

    public void refreshDuration()
    {
        Duration = 1.5f;
    }
}
