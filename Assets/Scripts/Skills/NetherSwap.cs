﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetherSwap : TargetedActiveSkill
{
    public override void lookForTarget()
    {
        base.lookForTarget();
    }

    private void Start()
    {
        setMaxRank(SkillValues.NETHER_SWAP_MAX_RANK);
    }

    public override void onActivate()
    {


        if (target.GetComponent<Unit>())
        {
            if (target.GetComponent<Hero>())
            {
                Debug.Log("CASTABLE");
                
                if (Vector3.Distance(target.transform.position, gameObject.transform.position) <= SkillValues.NETHER_SWAP_RANGE[rank] / 50)
                {
                    if (!GetComponent<ManaComponent>().checkForManaCost(SkillValues.NETHER_SWAP_MANA_COST[rank])) return;
                    base.onActivate();

                    cooldownTime = SkillValues.NETHER_SWAP_COOLDOWN;
                    Vector3 tempPosition;
                    tempPosition = transform.position;
                    transform.position = target.transform.position;
                    target.transform.position = tempPosition;
                }
            }
            else
            {
                Debug.Log("CANT TARGET ALLIES");
            }
        }
    }
}
