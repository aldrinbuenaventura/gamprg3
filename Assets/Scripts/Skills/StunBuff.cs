﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunBuff : Buff
{

    protected override void OnActivate(BuffReceiver target)
    {
        target.GetComponent<StatusCondition>().setStunned(true);
    }
    protected override void OnDeactivate()
    {
        Target.GetComponent<StatusCondition>().setStunned(false);
    }
}
