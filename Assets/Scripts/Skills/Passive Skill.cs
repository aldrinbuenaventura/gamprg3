﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveSkill : Skills
{
    public override void onKeyPress()
    {
        return;
    }

    public override void onRankUp()
    {
        base.onRankUp();
    }

    public override void rankedUp()
    {
        base.rankedUp();
        if (rank == 1) onActivate();
    }
}
