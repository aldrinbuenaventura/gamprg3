﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveOfTerror : TargetedActiveSkill
{
    public GameObject ProjectilePrefab;
    public GameObject ArmorDebuffEffect;

    public override void lookForTarget()
    {
        base.lookForTarget();
    }

    public void Start()
    {
        setMaxRank(SkillValues.WAVE_OF_TERROR_MAX_RANK);
    }

    public override void onActivate()
    {

        if (target.tag == "Ground")
        {
            Debug.Log("CASTABLE");

            if (Vector3.Distance(hitPoint, gameObject.transform.position) <= SkillValues.WAVE_OF_TERROR_CAST_RANGE / 50)
            {
                if (!GetComponent<ManaComponent>().checkForManaCost(SkillValues.WAVE_OF_TERROR_MANA_COST[rank]))
                {
                    Debug.Log("NO MANA");
                    return;
                }
                base.onActivate();

                GetComponent<AbilityManager>().callInvoke(SkillValues.WAVE_OF_TERROR_COOLDOWN[rank], 2);

                cooldownTime = SkillValues.WAVE_OF_TERROR_COOLDOWN[rank];
                Projectile projectile = Instantiate(ProjectilePrefab.GetComponent<Projectile>(), transform.position, Quaternion.identity);
                projectile.OnHit.AddListener(onProjectileHit);
                projectile.onSpawnLinear(hitPoint, SkillValues.WAVE_OF_TERROR_PROJECTILE_SPEED / GameValues.PROJECTILE_SPEED_VARIABLE);
            }
            else return;

        }
        else
        {
            Debug.Log("CANT TARGET UNITS");
        }
    }

    public void onProjectileHit(HealthComponent targetHP)
    {
        if (targetHP.GetComponent<Faction>().isRadiant != GetComponent<Faction>().isRadiant)
        {
            targetHP.TakeDamage(SkillValues.WAVE_OF_TERROR_DAMAGE[rank], "Magical", targetHP.GetComponent<DefenseComponent>(), gameObject);

            //Apply Armor Debuff
            BuffReceiver buffReceiver = targetHP.GetComponent<BuffReceiver>();
            if (buffReceiver)
            {
                Buff buff = Instantiate(ArmorDebuffEffect.GetComponent<Buff>());
                buff.setDuration(SkillValues.WAVE_OF_TERROR_DEBUFF_DURATION);
                buff.setRank(rank);
                buffReceiver.ApplyBuff(buff);
            }
        }
    }
}
