﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffDetails : MonoBehaviour
{
    public GameObject buffer;
    public int rank;

    public void setBuffer(GameObject go) { buffer = go; }
    public GameObject getBuffer() { return buffer; }

    public void setRank(int r) { rank = r; }
    public int getRank() { return rank; }

    public void setBufferRank(GameObject go, int r)
    {
        buffer = go;
        rank = r;
    }
}
