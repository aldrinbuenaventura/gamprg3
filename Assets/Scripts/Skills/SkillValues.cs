﻿public static class SkillValues
{
    public static float[] MAGIC_MISSILE_DAMAGE = { 0, 100, 175, 250, 325 };
    public static float[] MAGIC_MISSILE_STUN_DURATION = { 0, 1.4f, 1.5f, 1.6f, 1.7f };
    public static float[] MAGIC_MISSILE_COOLDOWN = { 0, 12, 11, 10, 9 };
    public static float[] MAGIC_MISSILE_MANA_COST = { 0, 100, 110, 120, 130 };
    public static float MAGIC_MISSILE_PROJECTILE_SPEED = 900;
    public static float MAGIC_MISSILE_CAST_RANGE = 500;
    public static int MAGIC_MISSILE_MAX_RANK = 4;

    public static float[] WAVE_OF_TERROR_DAMAGE = { 0, 60, 80, 100, 120 };
    public static float WAVE_OF_TERROR_DEBUFF_DURATION = 8;
    public static float[] WAVE_OF_TERROR_ARMOR_REDUCTION = { 0, -3, -4, -5, -6 };
    public static float[] WAVE_OF_TERROR_COOLDOWN = { 0, 16, 14, 12, 10 };
    public static float[] WAVE_OF_TERROR_MANA_COST = { 0, 25, 30, 35, 40 };
    public static float WAVE_OF_TERROR_PROJECTILE_SPEED = 2000;
    public static float WAVE_OF_TERROR_CAST_RANGE = 1400;
    public static float WAVE_OF_TERROR_RADIUS = 300;
    public static float WAVE_OF_TERROR_TRAVEL_DISTANCE = 1400;
    public static int WAVE_OF_TERROR_MAX_RANK = 4;

    public static float[] VENGEANCE_AURA_RANGE_BONUS = { 0, 50, 75, 100, 125 };
    public static float[] VENGEANCE_AURA_ATTRIBUTE_BONUS = { 0, 4, 7, 10, 13 };
    public static float VENGEANCE_AURA_RADIUS = 1200f;
    public static int VENGEANCE_AURA_MAX_RANK = 4;

    public static int ATTRIBUTE_MAX_RANK = 10;

    public static float[] TOWER_PROTECTION_AURA_BONUS = { 0, 3, 5 , 5, 5 };
    public static float TOWER_PROTECTION_AURA_RADIUS = 900f;


    public static float[] NETHER_SWAP_RANGE = { 0, 700, 850, 1000 };
    public static float NETHER_SWAP_COOLDOWN = 45;
    public static float[] NETHER_SWAP_MANA_COST = { 0, 100, 150, 200 };
    public static int NETHER_SWAP_MAX_RANK = 4;
}
