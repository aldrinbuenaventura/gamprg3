﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetedActiveSkill : ActiveSkill
{
    public GameObject target;
    public Vector3 hitPoint;
    public AbilityManager AbilityManagerGO;

    private void Awake()
    {
        AbilityManagerGO = GetComponent<AbilityManager>();
    }

    public override void onKeyPress()
    {
        base.onKeyPress();
        if (rank == 0) return;
        if (!isCooldown) lookForTarget();
    }

    public virtual void lookForTarget()
    {
        AbilityManagerGO.setLookingFor(this);
    }

    public override void onTargetFind(GameObject go , Vector3 point)
    {
        AbilityManagerGO.setLookingFor(null);
        hitPoint = point;
        target = go;
        onActivate();
    }

    public override void onActivate()
    {
        base.onActivate();
        isCooldown = true;
    }

}
