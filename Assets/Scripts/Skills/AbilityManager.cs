﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onCooldown : UnityEvent<float, int> { }
public class onSkillPointChange : UnityEvent<int> { }
public class AbilityManager : MonoBehaviour
{
    public onSkillPointChange OnSkillPointChange = new onSkillPointChange();
    public onCooldown OnCooldown = new onCooldown();

    [SerializeField]
    private Skills[] skills;

    public int skillPoints = 1;
    public int currentSkillIndex;

    int layer_mask;
    public Skills skillLookingForTarget;

    private void Start()
    {
        layer_mask = LayerMask.GetMask("Ground", "Building", "Unit");

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentSkillIndex = 0;
            skills[0].onKeyPress();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            currentSkillIndex = 1;
            skills[1].onKeyPress();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            currentSkillIndex = 2;
            skills[2].onKeyPress();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            currentSkillIndex = 3;
            skills[3].onKeyPress();
        }
        if (skillLookingForTarget)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;

                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, layer_mask))
                {
                    skillLookingForTarget.onTargetFind(hit.collider.gameObject, hit.point);
                }
            }
            if(Input.GetMouseButton(1))
            {
                setLookingFor(null);
            }
        }

        if(Input.GetKeyDown(KeyCode.U))
        {
            for (int i = 0; i < 4; i++)
            {
                skills[i].isCooldown = false;
                OnCooldown.Invoke(0.01f, i);
            }
            
        }
    }

    public void setLookingFor(Skills skill)
    {
        skillLookingForTarget = skill;
    }

    public void rankSkill(int index)
    {
        if (skillPoints == 0) return;
        //if (index == 4)
        //{
        //    GetComponent<HeroStats>().add
        //}
        skills[index].onRankUp();
    }

    public void addSkillPoints()
    {
        skillPoints++;
        OnSkillPointChange.Invoke(skillPoints);
    }

    public void useSkillPoint()
    {
        skillPoints--;
        OnSkillPointChange.Invoke(skillPoints);
    }

    public int getSkillPoints()
    {
        return skillPoints;
    }

    public void callInvoke(float cooldown, int index)
    {
        OnCooldown.Invoke(cooldown, currentSkillIndex);
    }
}
