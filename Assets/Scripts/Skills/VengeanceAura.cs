﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VengeanceAura : AuraPassiveSkill
{
    public GameObject VengeanceAuraEffect;
    Vengeance vengeance;

    public override void onActivate()
    {
        base.onActivate();
    }

    //Vengeance Aura Application
    Collider[] targets;
    int layer_mask;

    private void Start()
    {
        layer_mask = LayerMask.GetMask("Unit");
        setMaxRank(SkillValues.VENGEANCE_AURA_MAX_RANK);
    }


    public override void applyAura()
    {
        targets = Physics.OverlapSphere(this.gameObject.transform.position, SkillValues.VENGEANCE_AURA_RADIUS / 50, layer_mask);
        foreach (var target in targets)
        {
            vengeance = null;
            if (target.GetComponent<Faction>().isRadiant == this.GetComponent<Faction>().isRadiant)
            {
                BuffReceiver buffReceiver = target.GetComponent<BuffReceiver>();
                if(buffReceiver)
                {
                    if (buffReceiver.GetComponent<Vengeance>()) vengeance = buffReceiver.GetComponent<Vengeance>();
                    if (vengeance)
                    {
                        if(vengeance.getRank() == rank) vengeance.getBuffer().GetComponent<VengeanceAuraBuff>().refreshDuration();
                        else vengeance.getBuffer().GetComponent<VengeanceAuraBuff>().Replace();
                    }
                    else
                    {
                        Buff buff = Instantiate(VengeanceAuraEffect.GetComponent<Buff>());
                        buff.setRank(rank);
                        buffReceiver.ApplyBuff(buff);
                        buffReceiver.gameObject.AddComponent<Vengeance>().setBufferRank(buff.gameObject, rank);
                    }
                }
            }
        }

    }
}
