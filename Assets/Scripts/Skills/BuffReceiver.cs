﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour
{
    public List<Buff> buffs = new List<Buff>();

    public void ApplyBuff(Buff buff)
    {
        //checkBuff(buff);
        buffs.Add(buff);
        buff.Activate(this);
    }

    public void RemoveBuff(Buff buff)
    {
        buffs.Remove(buff);
        buff.Deactivate();
    }

    public void ReplaceBuff(Buff buff)
    {
        buffs.Remove(buff);
    }

    //public void checkBuff(Buff buff)
    //{
    //    Debug.Log(buff);
    //    foreach(Buff Buff in buffs)
    //    {
    //        Debug.Log(Buff);
    //        if (Buff == buff) Debug.Log("REPEAT");
    //    }
    //}
}
