﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Skills : MonoBehaviour
{


    public bool isActive;
    public bool isUltimate;
    public bool isCooldown = false;
    public int rank;
    public int maxRank;

    public virtual void onKeyPress() { }
    public virtual void onActivate()
    {
    }
    public virtual void onRankUp()
    {
        if (rank >= maxRank) return;

        int levelRequirement;
        if (!isUltimate) levelRequirement = GameValues.BASIC_SKILLS_LEVEL_REQUIREMENT[rank];
        else levelRequirement = GameValues.ULTIMATE_SKILLS_LEVEL_REQUIREMENT[rank];

        if (GetComponent<Level>().getLevel() >= levelRequirement)
        {
            GetComponent<AbilityManager>().useSkillPoint();
            rank++;
            rankedUp();
        }
    }

    public virtual void rankedUp() { }
    public virtual void onTargetFind(GameObject target, Vector3 point) { }
    public void setMaxRank(int i) { maxRank = i; }
    //public virtual void lookForTarget() { }
}
