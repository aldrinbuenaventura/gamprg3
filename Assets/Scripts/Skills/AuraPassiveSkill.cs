﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuraPassiveSkill : PassiveSkill
{
    public override void onActivate()
    {
        base.onActivate();

        InvokeRepeating("applyAura", 0f, 1f);
    }


    public virtual void applyAura()
    {
    }
}