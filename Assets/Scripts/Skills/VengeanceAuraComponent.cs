﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VengeanceAuraComponent : SkillComponent
{
    Collider[] targets;
    //public List<GameObject> inRange = new List<GameObject>();
    int layer_mask;

    private void Start()
    {
        layer_mask = LayerMask.GetMask("Unit");
        InvokeRepeating("applyAura", 0f, 1f);
    }

    public void applyAura()
    {
        //sightRangeWire = sightRange;
        targets = Physics.OverlapSphere(this.gameObject.transform.position, 1200 / 40, layer_mask);
        //inRange.Clear();
        foreach (var target in targets)
        {
            if (target.GetComponent<Faction>().isRadiant == this.GetComponent<Faction>().isRadiant) Debug.Log(target.gameObject);
        }

    }
}
