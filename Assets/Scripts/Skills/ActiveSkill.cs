﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSkill : Skills
{

    //public bool isCooldown = false;
    public float cooldownTime;

    private void Update()
    {
        if (isCooldown)
        {
            cooldownTime -= Time.deltaTime;
            if (cooldownTime <= 0) isCooldown = false;
        }
    }

    public override void onKeyPress()
    {
        base.onKeyPress();
    }

    public override void onActivate()
    {
        base.onActivate();
    }
}
