﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VengeanceAuraBuff : Buff
{
    protected override void OnActivate(BuffReceiver target) {
        if(target.GetComponent<AttackComponent>().getRangeType()) target.GetComponent<AttackComponent>().addRange(SkillValues.VENGEANCE_AURA_RANGE_BONUS[rank]);
        if (target.GetComponent<HeroStats>())
        {
            target.GetComponent<HeroStats>().addPrimaryAttribute(SkillValues.VENGEANCE_AURA_ATTRIBUTE_BONUS[rank]);
        }
    }
    protected override void OnDeactivate() {
        Destroy(Target.GetComponent<Vengeance>());
        if (Target.GetComponent<AttackComponent>().getRangeType()) Target.GetComponent<AttackComponent>().addRange(-SkillValues.VENGEANCE_AURA_RANGE_BONUS[rank]);
        
        if (Target.GetComponent<HeroStats>())
        {
            Target.GetComponent<HeroStats>().addPrimaryAttribute(-SkillValues.VENGEANCE_AURA_ATTRIBUTE_BONUS[rank]);
        }
    }

    public void refreshDuration()
    {
        Duration = 1.5f;
    }
}
