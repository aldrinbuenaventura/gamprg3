﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorDebuff : Buff
{
    protected override void OnActivate(BuffReceiver target)
    {
        target.GetComponent<DefenseComponent>().addArmor(SkillValues.WAVE_OF_TERROR_ARMOR_REDUCTION[rank]);
    }
    protected override void OnDeactivate()
    {
        Target.GetComponent<DefenseComponent>().addArmor(-SkillValues.WAVE_OF_TERROR_ARMOR_REDUCTION[rank]);
    }
}
