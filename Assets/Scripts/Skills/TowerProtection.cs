﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProtection : AuraPassiveSkill
{
    public GameObject TowerProtectionAuraEffect;
    TowerProtected towerProtected;

    public override void onActivate()
    {
        base.onActivate();
        InvokeRepeating("applyAura", 0f, 1f);
    }

    //Tower Protection Aura Application
    Collider[] targets;
    int layer_mask;

    private void Start()
    {
        layer_mask = LayerMask.GetMask("Unit");
    }


    public override void applyAura()
    {
        targets = Physics.OverlapSphere(this.gameObject.transform.position, SkillValues.TOWER_PROTECTION_AURA_RADIUS / 50, layer_mask);
        foreach (var target in targets)
        {
            towerProtected = null;
            if (target.GetComponent<Faction>().isRadiant == this.GetComponent<Faction>().isRadiant)
            {
                BuffReceiver buffReceiver = target.GetComponent<BuffReceiver>();
                if (buffReceiver)
                {
                    if(buffReceiver.GetComponent<TowerProtected>()) towerProtected = buffReceiver.GetComponent<TowerProtected>();
                    if (towerProtected)
                    {
                        Debug.Log(towerProtected.gameObject);
                        if(towerProtected.getRank() == rank) towerProtected.getBuffer().GetComponent<ProtectionAuraBuff>().refreshDuration();
                        else towerProtected.getBuffer().GetComponent<ProtectionAuraBuff>().Replace();
                    }
                    else
                    {
                        Buff buff = Instantiate(TowerProtectionAuraEffect.GetComponent<Buff>());
                        buff.setRank(rank);
                        buffReceiver.ApplyBuff(buff);
                        buffReceiver.gameObject.AddComponent<TowerProtected>().setBufferRank(buff.gameObject, rank);
                    }
                }
            }
        }

    }
}
